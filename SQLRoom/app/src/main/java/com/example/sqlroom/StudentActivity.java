package com.example.sqlroom;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.sqlroom.dao.AppDB;
import com.example.sqlroom.model.Student;
import com.example.sqlroom.utils.Validator;


public class StudentActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText edtMaSV;
    private EditText edtHoTen;
    private EditText edtGioiTinh;
    private Button btnThem;
    private Student student = new Student();

    public static Intent getIntent(Context context, Student student) {
        Intent intent = new Intent(context, StudentActivity.class);
        if (student != null) {
            intent.putExtra(Student.class.getName(), student);
        }
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);
        Intent intent = getIntent();
        if (intent.hasExtra(Student.class.getName())) {
            student = (Student) intent
                    .getSerializableExtra(Student.class.getName());
        }
        initViews();
    }

    private void initViews() {
        edtMaSV = findViewById(R.id.edt_maSV);
        edtHoTen = findViewById(R.id.edt_hoTen);
        edtGioiTinh = findViewById(R.id.edt_GioiTinh);

        edtMaSV.setText(student.getMaSV());
        edtHoTen.setText(student.getHoTen());
        edtGioiTinh.setText(student.getGioiTinh());

        btnThem = findViewById(R.id.btn_them);
        btnThem.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (Validator.isEmpty(edtMaSV, edtHoTen, edtGioiTinh)) {
            return;
        }
        student.setMaSV(edtMaSV.getText().toString());
        student.setHoTen(edtHoTen.getText().toString());
        student.setGioiTinh(edtGioiTinh.getText().toString());

        if (student.getId() > 0) {
            AppDB.getInstance(this).getStudentDao().update(student);
        } else {
            AppDB.getInstance(this).getStudentDao().intsert();
        }
        setResult(RESULT_OK);
        finish();
    }
}