package com.example.sqlroom.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.sqlroom.model.Student;

import java.util.List;

@Dao
public interface StudentDAO {

    @Query("SELECT * FROM Student")
    List<Student> getAll();

//    @Query("SELECT * FROM Student WHERE  like maSV")
//    List<Student> getStudent(String name);

    @Insert
    void intsert(Student ... students);

    @Update
    void update(Student ... students);

    @Delete
    void delete(Student ... students);

}
