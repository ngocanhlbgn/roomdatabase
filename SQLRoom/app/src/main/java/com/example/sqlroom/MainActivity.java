package com.example.sqlroom;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;


import com.example.sqlroom.adapter.StudentAdapter;
import com.example.sqlroom.dao.AppDB;
import com.example.sqlroom.model.Student;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_STUDENT = 1;
    private RecyclerView lvStudent;
    private StudentAdapter adapter;
    private FloatingActionButton btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        loadData();
    }

    private void loadData() {
        adapter.setData(AppDB.getInstance(this).getStudentDao().getAll());
    }

    private void initView() {
        lvStudent = findViewById(R.id.lv_Student);
        btnAdd = findViewById(R.id.btn_add);
        adapter = new StudentAdapter(getLayoutInflater());
        lvStudent.setAdapter(adapter);
        btnAdd.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = StudentActivity.getIntent(this,null);
        startActivityForResult(intent, REQUEST_STUDENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_STUDENT && resultCode == RESULT_OK) {
            loadData();
        }
    }
}