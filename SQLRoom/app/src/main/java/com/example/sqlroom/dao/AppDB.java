package com.example.sqlroom.dao;

import android.content.Context;

import androidx.room.Dao;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.sqlroom.model.Student;


@Database(entities = {Student.class},version = 1)
public abstract class AppDB extends RoomDatabase {
    private static AppDB appDB;
    public static AppDB getInstance(Context context) {
        if (appDB == null) {
            appDB = Room.databaseBuilder(
                    context,
                    AppDB.class,
                    "Student_List"
            )
                    .allowMainThreadQueries()
                    .build();
        }
        return appDB;
    }

    public abstract StudentDAO getStudentDao();
}
