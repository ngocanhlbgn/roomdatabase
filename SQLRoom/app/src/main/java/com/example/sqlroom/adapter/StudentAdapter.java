package com.example.sqlroom.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sqlroom.R;
import com.example.sqlroom.model.Student;

import java.util.List;

public class StudentAdapter  extends RecyclerView.Adapter<StudentAdapter.StudentHolder> {
    private LayoutInflater inflater;
    private List<Student> data;

    public StudentAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }
    public void setData(List<Student> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public StudentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new StudentHolder(
                inflater.inflate(R.layout.rows, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull StudentAdapter.StudentHolder holder, int position) {
        holder.bindView(data.get(position));

    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class StudentHolder extends RecyclerView.ViewHolder {
        private TextView tvMaSV;
        private TextView tvHoTen;
        private TextView tvGioiTinh;

        public StudentHolder(@NonNull View itemView) {
            super(itemView);
            tvMaSV = itemView.findViewById(R.id.tv_maSV);
            tvHoTen = itemView.findViewById(R.id.tv_hoTen);
            tvGioiTinh = itemView.findViewById(R.id.tv_gioiTinh);
        }

        public void bindView(Student student) {
            tvMaSV.setText(student.getMaSV());
            tvHoTen.setText(student.getHoTen());
            tvGioiTinh.setText(student.getGioiTinh());
        }
    }
}